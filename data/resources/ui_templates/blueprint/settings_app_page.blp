using Gtk 4.0;
using Adw 1;

template SettingsAppPage : Adw.PreferencesPage {
  icon-name: "emblem-system-symbolic";
  title: "App";

  Adw.PreferencesGroup {
    title: "Application";

    Adw.ActionRow {
      title: _("Keep running");
      subtitle: _("Fetch updates in the background");
      activatable-widget: keep_running_switch;

      Switch keep_running_switch {
        margin-top: 12;
        margin-bottom: 12;
      }
    }

    Adw.ActionRow {
      title: _("Sync on startup");
      subtitle: _("Fetch updates when the App is launched");
      activatable-widget: startup_sync_switch;

      Switch startup_sync_switch {
        margin-top: 12;
        margin-bottom: 12;
      }
    }

    Adw.ActionRow {
      title: _("Sync on metered connection");
      subtitle: _("Fetch updates on mobile connections in the background");
      activatable-widget: metered_sync_switch;

      Switch metered_sync_switch {
        margin-top: 12;
        margin-bottom: 12;
      }
    }
  }

  Adw.PreferencesGroup {
    title: "Update Interval";

    Adw.ActionRow never_row {
      title: _("Manual");
      subtitle: _("No automatic synchronization");
      activatable-widget: manual_update_check;

      [prefix]
      CheckButton manual_update_check {
        valign: center;
      }
    }

    Adw.ComboRow predefined_sync_row {
      title: _("Synchronize every");
      activatable-widget: predefined_update_check;

      [prefix]
      CheckButton predefined_update_check {
        valign: center;
        group: manual_update_check;
      }
    }

    Adw.ActionRow custom_sync_row {
      title: _("Custom Interval");
      activatable-widget: custom_update_check;

      [prefix]
      CheckButton custom_update_check {
        valign: center;
        group: manual_update_check;
      }

      Entry custom_update_entry {
        placeholder-text: _("hh:mm:ss");
        max-width-chars: 0;
        margin-top: 12;
        margin-bottom: 12;
      }
    }
  }

  Adw.PreferencesGroup {
    title: "Data";

    Adw.ExpanderRow {
      title: _("User Data");
      activatable: false;

      [action]
      Label user_data_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
        label: _("0");

        styles [
          "dim-label",
        ]
      }

      Adw.ComboRow limit_articles_row {
        title: _("Keep Articles");
        subtitle: _("Refers to the time of sync");
      }
    }

    Adw.ExpanderRow {
      title: _("Cache");
      enable-expansion: true;

      [action]
      Label cache_label {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
        label: _("0");

        styles [
          "dim-label",
        ]
      }

      Adw.ActionRow {
        title: _("Webview");
        activatable: false;

        Button clear_cache_button {
          margin-start: 12;
          margin-end: 12;
          margin-top: 12;
          margin-bottom: 12;
          label: _("Clear");
          halign: center;

          styles [
            "destructive-action",
          ]
        }
      }
    }
  }
}
