use crate::content_page::ContentPage;
use glib::clone;
use gtk4::prelude::*;

#[derive(Clone, Debug)]
pub struct ResponsiveLayout {
    pub content_page: ContentPage,
}

impl ResponsiveLayout {
    pub fn new(content_page: &ContentPage) -> Self {
        let layout = ResponsiveLayout {
            content_page: content_page.clone(),
        };
        layout.setup_article_view_header();
        layout.setup_leaflet();
        layout.setup_flap();
        layout
    }

    fn setup_article_view_header(&self) {
        self.content_page
            .articleview_column()
            .header_squeezer()
            .connect_visible_child_notify(clone!(@strong self as this => @default-panic,move |squeezer| {
                this.content_page
                    .articleview_column()
                    .footer_revealer()
                    .set_reveal_child(squeezer.visible_child().is_none());
            }));
    }

    fn setup_flap(&self) {
        self.content_page.flap().connect_folded_notify(clone!(@strong self as this => @default-panic, move |flap| {
            this.content_page.article_list_column().show_sidebar_button().set_visible(flap.is_folded());
            this.content_page.article_list_column().headerbar().set_show_start_title_buttons(flap.is_folded() && !flap.reveals_flap());
        }));

        self.content_page
            .article_list_column()
            .show_sidebar_button()
            .connect_toggled(clone!(@strong self as this => @default-panic, move |button| {
                this.content_page.flap().set_reveal_flap(button.is_active());
            }));

        self.content_page.flap().connect_reveal_flap_notify(clone!(@strong self as this => @default-panic, move |flap| {
            this.content_page.article_list_column().show_sidebar_button().set_active(flap.reveals_flap());
            this.content_page.article_list_column().headerbar().set_show_start_title_buttons(flap.is_folded() && !flap.reveals_flap());
        }));
    }

    fn setup_leaflet(&self) {
        self.content_page.leaflet().connect_folded_notify(
            clone!(@strong self as this => @default-panic, move |leaflet| {
                this.content_page.articleview_column().back_button().set_visible(leaflet.is_folded());
                this.content_page.article_list_column().headerbar().set_show_end_title_buttons(leaflet.is_folded());
                this.content_page.articleview_column().headerbar().set_show_start_title_buttons(leaflet.is_folded());
            }),
        );

        self.content_page.articleview_column().back_button().connect_clicked(
            clone!(@strong self as this => @default-panic, move |_button| {
                let article_list_column = this.content_page.article_list_column();
                this.content_page.leaflet().set_visible_child(article_list_column);
            }),
        );
    }

    pub fn show_sidebar(&self) {
        self.content_page
            .article_list_column()
            .show_sidebar_button()
            .set_active(true);
    }

    pub fn show_article_view(&self) {
        let leaflet = self.content_page.leaflet();
        if leaflet.is_folded() {
            let articleview_column = self.content_page.articleview_column();
            self.content_page.leaflet().set_visible_child(articleview_column);
        } else {
            let articlelist_column = self.content_page.article_list_column();
            self.content_page.leaflet().set_visible_child(articlelist_column);
        };
    }
}
